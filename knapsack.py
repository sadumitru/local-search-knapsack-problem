import random
from itertools import groupby, product


class BoundedQueue:
    values = []

    def __init__(self, max_capacity):
        self.max_capacity = max_capacity

    def __len__(self):
        return len(self.values)

    def put(self, value):
        if len(self.values) >= self.max_capacity:
            self.values.pop(0)
        self.values.append(value)


class Item:
    def __init__(self, weight, profit):
        self.weight = weight
        self.profit = profit


class KnapsackProblem:
    def __init__(self, max_capacity, number_of_items, items):
        self.max_capacity = max_capacity
        self.number_of_items = number_of_items
        self.items = items


def read_item_list(file_name):
    try:
        with open(file_name, 'r') as input_file:
            data = input_file.read()
            split_data = data.split("\n")
            profits = split_data[0].split(" ")
            weights = split_data[1].split(" ")
            return list(map(lambda p, w: Item(int(p), int(w)), profits, weights))
    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))


def get_probability_vector(number_of_items):
    zero_count = random.randint(0, number_of_items / 2)
    one_count = number_of_items - zero_count
    my_list = zero_count * [0] + one_count * [1]
    random.shuffle(my_list)
    return my_list


def solution_has_improved(results_list):
    is_increasing_list = all(i >= j for i, j in zip(results_list, results_list[1:]))
    g = groupby(results_list)
    all_elems_equal = next(g, True) and not next(g, False)
    return is_increasing_list and not all_elems_equal


def get_profit(current_solution, items_list):
    profit = 0
    for i in range(len(items_list) - 1):
        profit += current_solution[i] * items_list[i].profit
    return profit


def get_weight(current_solution, items_list):
    weight = 0
    for i in range(len(items_list)):
        weight += current_solution[i] * items_list[i].weight
    return weight


def is_valid_solution(solution, items_list, max_weight):
    return get_weight(solution, items_list) < max_weight


def is_better_solution(solution1, solution2, items_list):
    return get_profit(solution1, items_list) > get_profit(solution2, items_list)


def get_candidate_solutions(current_solution):
    possible_tuple_solutions = list(product(range(2), repeat=len(current_solution)))
    possible_solutions = [list(tup) for tup in possible_tuple_solutions]
    current_solution_index = possible_solutions.index(current_solution)
    current_solution_neighbors = possible_solutions[current_solution_index + 1:] + possible_solutions[
                                                                                   :current_solution_index][::-1]
    return current_solution_neighbors


def local_search(random_state, items_list, max_weight):
    number_of_elements = len(random_state)
    best_solution = random_state
    possible_solutions = get_candidate_solutions(best_solution)
    initial_profit = get_profit(best_solution, items_list)
    improvement_list = BoundedQueue(number_of_elements)
    improvement_list.put(initial_profit)
    for candidate_solution in possible_solutions:
        if len(improvement_list) > number_of_elements and solution_has_improved(improvement_list):
            return best_solution
        if is_valid_solution(candidate_solution, items_list, max_weight):
            improvement_list.put(get_profit(candidate_solution, items_list))
            if is_better_solution(candidate_solution, best_solution, items_list):
                best_solution = candidate_solution
    return best_solution


def main():
    items = read_item_list("./data.txt")
    number_of_items = 4
    knapsack1 = KnapsackProblem(
        max_capacity=8,
        number_of_items=number_of_items,
        items=items[:number_of_items]
    )

    random_state = get_probability_vector(knapsack1.number_of_items)
    while not is_valid_solution(random_state, knapsack1.items, knapsack1.max_capacity):
        random_state = get_probability_vector(knapsack1.number_of_items)
    local_search_result = local_search(random_state, knapsack1.items, knapsack1.max_capacity)
    for i in range(number_of_items):
        if local_search_result[i] != 0:
            print("Item %d with weight: %d and profit: %d was added to the knapsack" % (
                i, knapsack1.items[i].weight, knapsack1.items[i].profit))


main()
